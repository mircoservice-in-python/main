import os
import sys
import json
import pika
import logging
from dotenv import load_dotenv

from app import Product
from app import db

load_dotenv()
logging.basicConfig(stream=sys.stdout, level=logging.INFO)
logger = logging.getLogger(__name__)

params     = pika.URLParameters(os.environ.get('RABBITMQ_URL'))
connection = pika.BlockingConnection(params)
channel    = connection.channel()
QUEUE_NAME = 'app'

channel.queue_declare(queue=QUEUE_NAME)

def callback(channel, method, properties, body):
    logger.info("Received '"+properties.content_type+"' at " + QUEUE_NAME)
    data = json.loads(body)
    print(data)

    if properties.content_type == 'product.created':
        product = Product(
            id=data['id'],
            title=data['title'],
            image=data['image']
        )
        db.session.add(product)
        db.session.commit()
        logger.info("Product has been created!")

    elif properties.content_type == 'product.updated':
        product = Product.query.get(data['id'])
        product.title = data['title']
        product.image = data['image']
        db.session.commit()
        logger.info("Product has been updated!")

    elif properties.content_type == 'product.destroyed':
        product = Product.query.get(data)
        db.session.delete(product)
        db.session.commit()
        logger.info("Product has been destroyed!")


print("Started consuming "+QUEUE_NAME+"...")

channel.basic_consume(queue=QUEUE_NAME, on_message_callback=callback, auto_ack=True)
channel.start_consuming()
channel.close()
