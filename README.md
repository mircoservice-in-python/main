# Main API

A flask project

Example of usage:

```sh
:~$ docker-compose exec app sh
:# python manage.py db --help

usage: Perform database migrations
Perform database migrations

positional arguments:
  {init,revision,migrate,edit,merge,upgrade,downgrade,show,history,heads,branches,current,stamp}
    init                Creates a new migration repository
    revision            Create a new revision file.
    migrate             Alias for 'revision --autogenerate'
    edit                Edit current revision.
    merge               Merge two revisions together. Creates a new migration file
    upgrade             Upgrade to a later version
    downgrade           Revert to a previous version
    show                Show the revision denoted by the given symbol.
    history             List changeset scripts in chronological order.
    heads               Show current available heads in the script directory
    branches            Show current branch points
    current             Display the current revision for each database.
    stamp               'stamp' the revision table with the given revision; don't run any migrations

optional arguments:
  -?, --help            show this help message and exit

:# python manager.py db init
:# python manager.py db migrate
:# python manager.py db upgrade
```
