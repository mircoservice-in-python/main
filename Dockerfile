FROM python:3.9

ENV PYTHONUNBUFFERED 1
RUN mkdir -p /var/www/app
WORKDIR /var/www/app

COPY requirements.txt /var/www/app/requirements.txt

RUN pip install -r requirements.txt

COPY . /var/www/app
