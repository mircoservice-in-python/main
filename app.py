import os

from dotenv import load_dotenv
from dataclasses import dataclass
from flask import Flask
from flask import jsonify
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import UniqueConstraint
from sqlalchemy.dialects.mysql import INTEGER

load_dotenv()


app = Flask(__name__)
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = True
app.config["SQLALCHEMY_DATABASE_URI"] = os.environ.get('DATABASE_URL')
db = SQLAlchemy(app)

CORS(app)


#TODO: Move model code to other file
"""
Product (read-only)
"""
@dataclass
class Product(db.Model):
    id: int
    title: str
    image: str

    id    = db.Column(db.Integer, primary_key=True, autoincrement=False)
    title = db.Column(db.String(200))
    image = db.Column(db.String(200))
    # likes = db.Column(db.Integer)

@dataclass
class ProductUser(db.Model):
    id: int
    user_id: int
    product_id: int

    id         = db.Column(db.Integer, primary_key=True)
    user_id    = db.Column(db.Integer)
    product_id = db.Column(db.Integer)
    UniqueConstraint('user_id', 'product_id', name='user_product_unique')


@app.route("/")
def index():
    return "It' working"

@app.route("/api/products")
def list_products():
    return jsonify(Product.query.all())

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
