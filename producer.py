import os
import pika
from dotenv import load_dotenv

load_dotenv()

params     = pika.URLParameters(os.environ.get('RABBITMQ_URL'))
connection = pika.BlockingConnection(params)
channel    = connection.channel()

def publish():
    channel.basic_publish(exchange='', routing_key='admin', body="hello from flask app!")
